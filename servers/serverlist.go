package servers

type Host struct {
	IP        string
	Latitude  float64
	Longitude float64
	City      string
	Country   string
}

type ServerList interface {
	Continents() []string
	HostsForContinent(cc string) []Host
}
