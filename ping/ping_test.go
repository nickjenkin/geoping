package ping

import (
	"os"
	"testing"
)

func TestPing(t *testing.T) {

	if os.Geteuid() != 0 {
		t.Skip("ping test requires root")
	}

	host := "127.0.0.1"

	dur, err := Ping(host)

	if err != nil {
		t.Error(err)
	}

	if dur.Seconds() > 1 {
		t.Error("ping taking to long")
	}
}
