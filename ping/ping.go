package ping

import (
	"bytes"
	//"fmt"
	"errors"
	"math"
	"net"
	"os"
	"time"
)

func PingSequence(host string, sequence int) (time.Duration, error) {
	c, err := net.Dial("ip4:icmp", host)
	if err != nil {
		return 0, err
	}

	typ := icmpv4EchoRequest
	c.SetDeadline(time.Now().Add(2000 * time.Millisecond))
	defer c.Close()

	xid, xseq := os.Getpid()&0xffff, sequence+1

	b, err := (&icmpMessage{
		Type: typ, Code: 0,
		Body: &icmpEcho{
			ID: xid, Seq: xseq,
			Data: bytes.Repeat([]byte("GEOPING"), 5),
		},
	}).Marshal()

	if err != nil {
		return 0, err
	}

	start := time.Now()

	if _, err := c.Write(b); err != nil {
		return 0, err
	}

	var m *icmpMessage
	var end time.Time
	for {
		if _, err := c.Read(b); err != nil {
			return -1, err
		}
		end = time.Now()
		b = ipv4Payload(b)
		if m, err = parseICMPMessage(b); err != nil {
			return -2, err
		}
		switch m.Type {
		case icmpv4EchoRequest, icmpv6EchoRequest:
			continue
		}
		break
	}
	/*switch p := m.Body.(type) {
	case *icmpEcho:
		fmt.Printf("got id=%v, seqnum=%v; expected id=%v, seqnum=%v\n", p.ID, p.Seq, xid, xseq)
	default:
		fmt.Println("got type=%v, code=%v; expected type=%v, code=%v", m.Type, m.Code, typ, 0)
	}*/

	return end.Sub(start), nil
}

func Ping(host string) (time.Duration, error) {
	return PingSequence(host, 1)
}
