package distance

import (
	"geoping/ping"
	"time"
)

func DistanceToHost(host string, tolerance float64) (DistanceKM, error) {

	minPing, count := pingMinimum(host, 5)

	if count < 4 {
		return 0, errors.New("high packet loss occuring while pinging host")
	}

	return Distance(minPing, tolerance), nil
}

func pingMinimum(host string, count int) (min time.Duration, success int) {
	min = time.Hour * 500
	success = 0
	for i := 0; i < count; i++ {
		dur, err := ping.PingSequence(host, i)

		if err == nil {
			if dur.Seconds() < min.Seconds() {
				min = dur
				success++
			}
		}
	}
	return min, success
}
