package distance

import (
	"time"
)

type DistanceKM float64

// The longer the distance travelled, more time is spent on high speed fibre
// The shorter the distance there is a large impact from lastmile services

func Distance(ping time.Duration, tolerance float64) DistanceKM {

	// The speed of light is ~200,000km/s in fibre due to refraction.
	c := DistanceKM(200000) // km/s

	// adjust for lastmile latency, hardware, non-direct routes
	// this is a very scientific guess based on research commonly referred
	// to as "pulling numbers out of thin air"
	// 0.7 is a good number
	c = c * tolerance

	distance := ping.Seconds() * c // distance travelled (km)
	return distance
}
