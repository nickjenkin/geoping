GeoPing
=======

Pings remote servers in known locations to triangulate your location.


Notes
-----

* Will not work if you are using a TI-83 as a computer.
