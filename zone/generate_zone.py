#!/usr/bin/python

import csv
import urlparse
import socket
import xml.etree.ElementTree as ET
import json

"""
root.geoping.me TXT -> list of continent codes (TTL:48h)

<CC>.continents.geoping.me TXT -> list of country codes (TTL: 48h)

<ISO>.countries.geoping.me TXT -> server count (TTL: 60s)

1-MAX.ISO.countries.geoping.me -> A = ip, TXT = long/lat/city (TTL: 48h)



"""
dnscache = {}

def dnscache_load():
    global dnscache
    with open('dnscache.json', 'rb') as f:
        dnscache = json.load(f)

        f.close()

def dnscache_write():

    f = open('dnscache.json', 'wb')
    f.write(json.dumps(dnscache))
    f.close()

def dnscache_get(host):
    global dnscache
    if host not in dnscache:
        print "resolving %s" % url

        dnscache[host] = socket.gethostbyname(host)
        dnscache_write()
    return dnscache[host]

def main():
    root = "root.nickjenkin.com"
    ttl = "1h"

    dnscache_load()

    servers = load_server_list('../tmp/servers.xml')
    conts = load_continent_map('../tmp/continents.csv')

    countries = {}
    continents = {}


    for server in servers:
        if server.country not in conts:
            continue
        cont = conts[server.country]

        if server.country in conts:
            cont = conts[server.country]

            if server.country not in countries:
                countries[server.country] = []

            if cont not in continents:
                continents[cont] = {}

            if server.country not in continents[cont]:
                continents[cont][server.country] = []

            continents[cont][server.country].append(server)
            #continents[cont].append(server.country)

    create_zone(root, ttl, servers, countries, continents)


def create_zone(root, ttl, servers, countries, continents):
    print_zone_header(root, ttl)

    print_cont_zone(continents)


def print_zone_header(root, ttl):
    print "$ORIGIN %s." % (root)
    print "$TTL %s" % (ttl)
    print ""


def print_cont_zone(continents):


    txt = " ".join(continents.keys())
    print_record("TXT", "continents", txt)

    for cont in continents:
        print ""
        print_record("A", "%s.continents" % cont, "127.0.0.1")

        txt = " ".join(continents[cont].keys())
        print_record("TXT", "%s.continents" % cont, txt)

        for country in continents[cont]:
            #print_record("A", "%s.countries" % country, "127.0.0.1")
            print_record("TXT", "%s.countries" % country, len(continents[cont][country]))

            i = 0
            for server in continents[cont][country]:
                print_record("A", "%d.%s.countries" % (i, country), server.host)
                print_record("TXT", "%d.%s.countries" % (i, country), "%s;%s;%s" % (server.latitude, server.longitude, server.city))
                i += 1

def print_record(type, host, val, ttl=3600):
    if type == "TXT":
        val = repr(val)

    print "%s\t%d\tIN\t%s\t%s" % (host, ttl, type, val)


def load_continent_map(filename):
    result = {}
    with open(filename, 'rb') as csvfile:
        cr = csv.reader(csvfile, delimiter=' ')

        for row in cr:

            if len(row) < 2:
                continue

            iso = row[1]
            cont = row[0]
            result[iso] = cont


    return result


class Server:
    host = ''
    country = ''
    city = ''
    longitude = 0
    latitude = 0

    def __str__(self):
        return self.host


def parse_server_host(url):

    result = urlparse.urlparse(url)

    try:
        return dnscache_get(result.hostname)

    except:
        return False


def load_server_list(filename):

    servers = []

    tree = ET.parse(filename)

    root = tree.getroot()

    for server in root.iter('server'):
        s = Server()
        attrs = server.attrib

        s.host = parse_server_host(attrs['url'])
        s.country = attrs['cc']
        s.city = attrs['name']
        s.longitude = attrs['lon']
        s.latitude = attrs['lat']

        if not s.host:
            continue

        servers.append(s)
        #print s

    return servers


if __name__ == "__main__":
    main()
