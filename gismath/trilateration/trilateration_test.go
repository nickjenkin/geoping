package trilateration

import (
	helpers "geoping/testing"
	"testing"
)

func TestTrilateration(t *testing.T) {

	lat0, long0, dist0 := 37.418436, -121.963477, 0.265710701754
	lat1, long1, dist1 := 37.417243, -121.961889, 0.234592423446
	lat2, long2, dist2 := 37.418692, -121.960194, 0.0548954278262
	elat, elong := 37.4191023738, -121.960579208

	lat, long := Trilateration(lat0, long0, dist0, lat1, long1, dist1, lat2, long2, dist2)
	t.Logf("lat = %f, long = %f", lat, long)
	if !helpers.FloatsEqual(elat, lat, 6) || !helpers.FloatsEqual(elong, long, 6) {
		t.Errorf("expected lat/long [%f, %f] got [%f, %f]", elat, elong, lat, long)
	}

}
