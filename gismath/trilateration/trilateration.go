package trilateration

//http://gis.stackexchange.com/questions/66/trilateration-using-3-latitude-and-longitude-points-and-3-distances
import (
	"geoping/gismath"
	"math"
)

func Trilateration(lat0, long0, dist0,
	lat1, long1, dist1,
	lat2, long2, dist2 float64) (lat, long float64) {

	v1 := gismath.LatLongToECEFVector(lat0, long0)
	v2 := gismath.LatLongToECEFVector(lat1, long1)
	v3 := gismath.LatLongToECEFVector(lat2, long2)

	ex := v2.Subtract(v1).ScalarDivide(v2.Subtract(v1).Norm())

	i := ex.Dot(v3.Subtract(v1))

	itex := ex.ScalarMultiply(i)
	ey := v3.Subtract(v1).Subtract(itex).ScalarDivide(v3.Subtract(v1).Subtract(itex).Norm())

	ez := ex.Cross(ey)

	d := v2.Subtract(v1).Norm()

	j := ey.Dot(v3.Subtract(v1))

	x := (math.Pow(dist0, 2) - math.Pow(dist1, 2) + math.Pow(d, 2)) / (2 * d)
	y := ((math.Pow(dist0, 2) - math.Pow(dist2, 2) + math.Pow(i, 2) + math.Pow(j, 2)) / (2 * j)) - ((i / j) * x)

	z := math.Sqrt(math.Pow(dist0, 2) - math.Pow(x, 2) - math.Pow(y, 2))

	pt := v1.Add(ex.ScalarMultiply(x)).Add(ey.ScalarMultiply(y)).Add(ez.ScalarMultiply(z))

	pt[2] = pt[2] / gismath.EarthRadius

	lat = math.Asin(pt[2]) * gismath.RadToDeg
	long = math.Atan2(pt[1], pt[0]) * gismath.RadToDeg

	return
}
