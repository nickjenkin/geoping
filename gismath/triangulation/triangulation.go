package triangulation

import (
	"geoping/gismath"
)

type Triangulator struct {
	positions []*position
}

type position struct {
	lat  float64
	long float64
}

func New() *Triangulator {
	t := new(Triangulator)
	t.positions = make([]*position, 0)
	return t
}

func (t *Triangulator) AddPosition(lat, long float64) {
	t.positions = append(t.positions, &position{lat, long})
}

func (t *Triangulator) Triangulate() (lat, long float64) {
	var x_avg, y_avg, z_avg float64

	if len(t.positions) == 0 {
		return 0, 0
	}

	for _, pos := range t.positions {
		x, y, z := pos.Cartesian()

		x_avg += x
		y_avg += y
		z_avg += z
	}
	plen := float64(len(t.positions))
	x_avg = x_avg / plen
	y_avg = y_avg / plen
	z_avg = z_avg / plen

	return gismath.CartesianToLatLong(x_avg, y_avg, z_avg)
}

func (p *position) Cartesian() (x, y, z float64) {
	return gismath.LatLongToCartesian(p.lat, p.long)
}
