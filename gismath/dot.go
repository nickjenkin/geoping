package gismath

func Dot(v1, v2 []float64) (dotproduct float64) {

	if len(v1) != len(v2) {
		panic("Dot product requires vectors to be of the same length")
	}

	for i := 0; i < len(v1); i++ {
		dotproduct += v1[i] * v2[i]
	}

	return
}
