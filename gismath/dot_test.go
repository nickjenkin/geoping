package gismath

import (
	"testing"
)

func TestDotProduct(t *testing.T) {
	verifyDotProduct(t, []float64{2, 3, 5}, []float64{3, 4, 6}, 48)
}

func verifyDotProduct(t *testing.T, v1 []float64, v2 []float64, expected float64) {
	result := Dot(v1, v2)

	if result != expected {
		t.Errorf("expected %f got %f", expected, result)
	}
}
