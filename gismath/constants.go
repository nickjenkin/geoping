package gismath

import (
	"math"
)

const (
	EarthRadius float64 = 6371.0
)

const (
	RadToDeg  = 180 / math.Pi
	DegToRad  = math.Pi / 180
	RadToGrad = 200 / math.Pi
	GradToDeg = math.Pi / 200
)
