package gismath

import (
	"testing"
)

func xTestLatLongToCartesian1(t *testing.T) {
	// 37.418436,-121.963477
	var lat, long float64
	lat = 37.418436
	long = -121.963477
	x, y, z := LatLongToCartesian(lat, long)

	if x != -0.420442596 || y != -0.67380418 || z != 0.607631426 {
		t.Errorf("LatLongToCartesian(%d, %d): x=%d y=%d z=%d", lat, long, x, y, z)
	}
}
