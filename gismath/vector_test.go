package gismath

import (
	helpers "geoping/testing"
	"testing"
)

func TestVector(t *testing.T) {
	v := Vector([]float64{4, 5, 6})
	v2 := Vector([]float64{1, 2, 3})

	t.Log("vector=", v.Multiply(v2), v[1])

}

func TestVectorNorm(t *testing.T) {
	v := Vector{0.07631127, -0.14262403, -0.10535841}
	var expected string = fTos(0.193043)
	var result string = fTos(v.Norm())
	if result != expected {
		t.Errorf("expecting %s got %s", expected, result)
	}
}

func TestVectorCrossProduct(t *testing.T) {
	/*>>> x = [1, 2, 3]
	  >>> y = [4, 5, 6]
	  >>> np.cross(x, y)
	  array([-3,  6, -3])*/
	x := Vector{1, 2, 3}
	y := Vector{4, 5, 6}

	res := x.Cross(y)
	t.Log(x)
	t.Log(y)
	t.Log(res)
	expected := Vector{-3, 6, -3}

	if !expected.Equals(res) {
		t.Errorf("expecting cross product to be %f but got %f", expected, res)
	}
}

func TestVectorCrossProduct2(t *testing.T) {
	ex := Vector{0.39530807, -0.73882179, -0.54577824}
	ey := Vector{0.81668620, 0.01074473, 0.57698198}
	expected := Vector{-0.42042262, -0.67381519, 0.60763304}

	res := ex.Cross(ey)
	t.Log(res)
	t.Log(expected)

	if !vectorFuzzyEqual(expected, res) {
		t.Errorf("expecting cross product to be %f but got %f", expected, res)
	}
}

func vectorFuzzyEqual(v1, v2 Vector) bool {
	if len(v1) != len(v2) {
		return false
	}

	for i := 0; i < len(v1); i++ {
		if !helpers.FloatsEqual(v1[i], v2[i], 6) {
			return false
		}
	}
	return true
}
