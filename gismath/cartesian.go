package gismath

import (
	"math"
)

func LatLongToCartesian(lat, long float64) (x, y, z float64) {
	x = math.Cos(lat) * math.Cos(long)
	y = math.Cos(lat) * math.Sin(long)
	z = math.Sin(lat)

	return
}

func CartesianToLatLong(x, y, z float64) (lat, long float64) {

	lat = math.Atan(z / math.Sqrt((x*2)+(y*2)))
	long = math.Atan(y / x)

	return
}
