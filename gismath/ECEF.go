package gismath

// LatLongToECEF converts a lat/long value to set of Earth-Centered,
// Earth-Fixed (ECEF/ECR) values.
func LatLongToECEF(lat, long float64) (x, y, z float64) {
	lat = lat * DegToRad
	long = long * DegToRad

	x, y, z = LatLongToCartesian(lat, long)
	x *= EarthRadius
	y *= EarthRadius
	z *= EarthRadius
	return
}

func LatLongToECEFVector(lat, long float64) Vector {
	x, y, z := LatLongToECEF(lat, long)

	return Vector{x, y, z}
}
