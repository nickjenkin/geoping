package gismath

import (
	"strconv"
	"testing"
)

func TestLatLongToECEF(t *testing.T) {

	verifyECEF(t, 37.418436, -121.963477, -2678.63977972, -4292.80643221, 3871.21981629)
	verifyECEF(t, 37.417243, -121.961889, -2678.56346844, -4292.94905625, 3871.11445788)
	verifyECEF(t, 37.418692, -121.960194, -2678.38464539, -4292.94523407, 3871.24242442)

}

func verifyECEF(t *testing.T, lat, long, x, y, z float64) {
	x1, y1, z1 := LatLongToECEF(lat, long)

	t.Log(fTos(x), fTos(x1))
	if fTos(x) != fTos(x1) || fTos(y) != fTos(y1) || fTos(z) != fTos(z1) {
		t.Errorf("LatLongToCartesian(%f, %f): got:(x=%f, y=%f, z=%f) expected:(x=%f, y=%f, z=%f)",
			lat, long, x1, y1, z1, x, y, z)
	}
}

func fTos(f float64) string {
	return strconv.FormatFloat(f, 'f', 6, 64)
}
