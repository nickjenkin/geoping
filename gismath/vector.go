package gismath

import (
	"math"
)

type vectorMath func(val1, val2 float64) float64
type Vector []float64

func (v Vector) Multiply(m Vector) Vector {
	return v.mapFunc(m, valueMultiply)
}

func (v Vector) Add(m Vector) Vector {
	return v.mapFunc(m, valueAdd)
}

func (v Vector) Subtract(m Vector) Vector {
	return v.mapFunc(m, valueSub)
}

func (v Vector) Dot(m Vector) float64 {
	return Dot(v, m)
}

func (v Vector) ScalarMultiply(scalar float64) Vector {
	return v.mapScalarFunc(scalar, valueMultiply)
}

func (v Vector) ScalarAdd(scalar float64) Vector {
	return v.mapScalarFunc(scalar, valueAdd)
}

func (v Vector) ScalarSubtract(scalar float64) Vector {
	return v.mapScalarFunc(scalar, valueSub)
}

func (v Vector) ScalarDivide(scalar float64) Vector {
	return v.mapScalarFunc(scalar, valueDivide)
}
func (v Vector) Equals(v2 Vector) bool {
	if len(v) != len(v2) {
		return false
	}

	for i := 0; i < len(v); i++ {
		if v[i] != v2[i] {
			return false
		}
	}

	return true
}
func (v Vector) Cross(v2 Vector) Vector {

	r := Vector{v[1]*v2[2] - v[2]*v2[1],
		v[2]*v2[0] - v[0]*v2[2],
		v[0]*v2[1] - v[1]*v2[0]}

	return r
}

func (v Vector) Sum() (sum float64) {

	for _, val := range v {
		sum += val
	}
	return
}

func (v Vector) Norm() float64 {
	//sqrt(add.reduce((x.conj() * x).ravel().real))
	xx := v.Multiply(v)
	return math.Sqrt(xx.Sum())
}

func (v Vector) mapFunc(v2 Vector, f vectorMath) Vector {
	// a panic may be more apropriate here when the vector sizes don't match
	size := int(math.Min(float64(len(v)), float64(len(v2))))
	result := make([]float64, size)
	for i := 0; i < size; i++ {
		result[i] = f(v[i], v2[i])
	}

	return Vector(result)
}

func (v Vector) mapScalarFunc(scalar float64, f vectorMath) Vector {
	res := Vector(make([]float64, len(v)))
	for i := 0; i < len(v); i++ {
		res[i] = f(v[i], scalar)
	}

	return res
}

func valueMultiply(val1, val2 float64) float64 {
	return val1 * val2
}

func valueAdd(val1, val2 float64) float64 {
	return val1 + val2
}

func valueSub(val1, val2 float64) float64 {
	return val1 - val2
}

func valueDivide(val1, val2 float64) float64 {
	return val1 / val2
}
