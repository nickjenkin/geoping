package testing

import (
	"strconv"
)

func FloatsEqual(f1, f2 float64, fig int) bool {
	return strconv.FormatFloat(f1, 'f', fig, 64) == strconv.FormatFloat(f2, 'f', 6, 64)
}
