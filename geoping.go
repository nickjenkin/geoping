package main

import (
	"flag"
	"fmt"
	"geoping/ping"
	"net"

	"strings"
	"time"
)

func hasRawSocketPermissions() bool {

	_, err := net.Dial("ip4:icmp", "localhost")

	if err == nil {
		return true
	}
	return false
}

func main() {

	if !hasRawSocketPermissions() {
		fmt.Println("geoping needs to be run as root or with access to raw sockets")

		return
	}
	var servers = flag.String("servers", "127.0.0.1", "comma seperated list of IP addresses to ping")

	flag.Parse()
	serverList := strings.Split(*servers, ",")
	for _, host := range serverList {
		host = strings.Trim(host, " ")

		fmt.Print(host)

		for i := 0; i < 5; i++ {
			ms, err := ping.PingMinimum(host, 5)
			if err == nil {
				fmt.Printf(" %s", ms)
			} else {
				fmt.Print(" *")
			}
		}
		fmt.Println("")
		<-time.After(time.Millisecond * 100)
	}
}
