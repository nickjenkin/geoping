package net

type DistanceKM float64

// The longer the distance travelled, more time is spent on high speed fibre
// The shorter the distance there is a large impact from lastmile services

func Distance(ping uint64, tolerance float64) DistanceKM {

	// The speed of light is ~200,000km/s in fibre due to refraction.
	c := DistanceKM(200.0) // km/ms

	// adjust for lastmile latency, hardware, non-direct routes
	// this is a very scientific guess based on research commonly referred
	// to as "pulling numbers out of thin air"
	// 0.7 is a good number
	c = c * tolerance

	distance := ping * c // distance travelled (km)
	return distance
}
