package net

import (
	"math"
)

func Ping(addr string) (float64, error) {
	return 200.0, nil
}

func PingMinimum(addr string, repeat int) float64 {

	var min float64 = math.MaxFloat64

	for i := 0; i < repeat; i++ {
		ping, _ := Ping(addr)

		if ping < min {
			min = ping
		}
	}

	return min
}
